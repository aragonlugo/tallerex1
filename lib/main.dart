import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tallerex1/caracteristicas/bloc.dart';
import 'package:tallerex1/caracteristicas/repositorio_verificacion.dart';
import 'package:tallerex1/caracteristicas/verificacion/vista/nombre_confirmado.dart';
import 'package:tallerex1/caracteristicas/verificacion/vista/vista_actualizacion.dart';
import 'package:tallerex1/caracteristicas/verificacion/vista/vista_creandose.dart';
import 'package:tallerex1/caracteristicas/verificacion/vista/vista_esperando_nombre.dart';
import 'package:tallerex1/caracteristicas/verificacion/vista/nombre_no_confirmado.dart';
import 'package:cached_network_image/cached_network_image.dart';

void main() {
  runApp(const AplicacionInyectada());
}

class AplicacionInyectada extends StatelessWidget {
  const AplicacionInyectada({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) {
        BlocVerificacion blocVerificacion = BlocVerificacion(RepositorioReal());
        Future.delayed(const Duration(seconds: 2), () {
          blocVerificacion.add(Creado());
        });
        return blocVerificacion;
      },
      child: const Aplicacion(),
    );
  }
}

class Aplicacion extends StatelessWidget {
  const Aplicacion({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Builder(builder: (context) {
          var estado = context.watch<BlocVerificacion>().state;
          if (estado is Creandose) {
            return const VistaCreandose();
          }
          if (estado is SolicitandoNombre) {
            return const VistaSolicitandoNombre();
          }
          if (estado is MostrandoSolicitudActualizacion) {
            return const VistaMostrandoSolicitudActualizacion();
          }
          if (estado is MostrandoNombreNoConfirmado) {
            return VistaMostrandoNombreNoConfirmado(estado.nick);
          }
          if (estado is MostrandoNombreConfirmado) {
            return VistaMostrandoNombreConfirmado(estado.registroUsuario);
          }

          return const Center(
              child: Text('Si estas viendo esto algo salio mal, HUYE'));
        }),
      ),
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final title = 'Imágenes en caché';

    return MaterialApp(
      title: title,
      home: Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: Center(
          child: CachedNetworkImage(
            imageUrl: 'https://picsum.photos/250?image=9',
          ),
        ),
      ),
    );
  }
}
