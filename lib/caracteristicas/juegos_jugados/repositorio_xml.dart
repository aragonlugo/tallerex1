import 'dart:io';

import 'package:fpdart/fpdart.dart';
import 'package:tallerex1/caracteristicas/dominio/nick_formado.dart';
import 'package:tallerex1/caracteristicas/dominio/problemas.dart';
import 'package:xml/xml.dart';

abstract class RepositorioXml {
  Future<Either<Problema, List<String>>> obtenerXml(NickFormado nick);
}

class RepositorioXmlPruebas extends RepositorioXml {
  final tamanoPagina = 2;
  @override
  Future<Either<Problema, List<String>>> obtenerXml(NickFormado nick) async {
    if (!['Benthor', 'Fokuleh'].contains(nick.valor)) {
      return left(UsuarioNoRegistrado());
    }

    try {
      String elXml = File(
              './test/caracteristicas/juegos_jugados/${nick.valor.toLowerCase()}1.xml')
          .readAsStringSync();
      int cuantosPaginas = _obtenerCuantasPaginasDesdeXml(elXml);
      List<String> nombresPaginas =
          _obtenerNombresPaginas(cuantosPaginas, nick);
      return Right(
          nombresPaginas.map((e) => File(e).readAsStringSync()).toList());
    } catch (e) {
      return left(VersionIncorrectaXml());
    }
  }

  int _obtenerCuantasPaginasDesdeXml(String elXml) {
    XmlDocument documento = XmlDocument.parse(elXml);
    String cadenaNumero = documento.getElement('plays')!.getAttribute('total')!;
    int numero = int.parse(cadenaNumero);
    return (numero / tamanoPagina).ceil();
  }

  List<String> _obtenerNombresPaginas(int cuantasPaginas, NickFormado nick) {
    final base = './test/caracteristicas/juegos_jugados/${nick.valor}';
    List<String> lista = [];
    for (var i = 1; i <= cuantasPaginas; i++) {
      lista.add('$base$i.Xml');
    }
    return lista;
  }
}
