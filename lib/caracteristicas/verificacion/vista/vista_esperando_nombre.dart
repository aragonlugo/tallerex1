import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tallerex1/caracteristicas/bloc.dart';
import 'package:tallerex1/caracteristicas/dominio/nick_formado.dart';

class VistaSolicitandoNombre extends StatefulWidget {
  const VistaSolicitandoNombre({Key? key}) : super(key: key);

  @override
  State<VistaSolicitandoNombre> createState() => _VistaSolicitandoNombreState();
}

class _VistaSolicitandoNombreState extends State<VistaSolicitandoNombre> {
  final _controlador = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final elBloc = context.read<BlocVerificacion>();
    return Column(
      children: [
        const Text('Dame el nombre'),
        TextField(
          controller: _controlador,
        ),
        TextButton(
            onPressed: () {
              elBloc.add(
                  NombreRecibido(NickFormado.constructor(_controlador.text)));
            },
            child: const Text('Algo debe decir aqui'))
      ],
    );
  }

  @override
  void dispose() {
    _controlador.dispose();
    super.dispose();
  }
}
