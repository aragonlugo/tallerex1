import 'package:bloc/bloc.dart';
import 'package:tallerex1/caracteristicas/dominio/nick_formado.dart';
import 'package:tallerex1/caracteristicas/dominio/problemas.dart';
import 'package:tallerex1/caracteristicas/dominio/registro_usuario.dart';
import 'package:tallerex1/caracteristicas/repositorio_verificacion.dart';

class EventoVerificacion {}

class Creado extends EventoVerificacion {}

class NombreRecibido extends EventoVerificacion {
  final NickFormado nick;
  NombreRecibido(this.nick);
}

class NombreConfirmado extends EventoVerificacion {}

class EstadoVerificacion {}

class Creandose extends EstadoVerificacion {}

class SolicitandoNombre extends EstadoVerificacion {}

class MostrandoNombre extends EstadoVerificacion {}

class MostrandoNombreConfirmado extends EstadoVerificacion {
  final RegistroUsuario registroUsuario;
  MostrandoNombreConfirmado(this.registroUsuario);
}

class EsperandoConfirmacionNombre extends EstadoVerificacion {}

class MostrandoSolicitudActualizacion extends EstadoVerificacion {}

class MostrandoNombreNoConfirmado extends EstadoVerificacion {
  final NickFormado nick;
  MostrandoNombreNoConfirmado(this.nick);
}

class EsperandoNombre extends EstadoVerificacion {}

class BlocVerificacion extends Bloc<EventoVerificacion, EstadoVerificacion> {
  final RepositorioVerificacion _repositorioVerificacion;
  BlocVerificacion(this._repositorioVerificacion) : super(Creandose()) {
    on<Creado>((event, emit) {
      emit(SolicitandoNombre());
    });
    on<NombreRecibido>((event, emit) async {
      emit(EsperandoConfirmacionNombre());
      final resultado =
          await _repositorioVerificacion.obtenerRegistroUsuario(event.nick);
      resultado.match((l) {
        if (l is VersionIncorrectaXml) {
          emit(MostrandoSolicitudActualizacion());
        }
        if (l is UsuarioNoRegistrado) {
          emit(MostrandoNombreNoConfirmado(event.nick));
        }
      }, (r) {
        emit(MostrandoNombreConfirmado(r));
      });
    });
  }
}
