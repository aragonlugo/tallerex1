import 'package:flutter_test/flutter_test.dart';
import 'package:tallerex1/caracteristicas/dominio/juego_jugado.dart';
import 'package:tallerex1/caracteristicas/dominio/nick_formado.dart';
import 'package:tallerex1/caracteristicas/juegos_jugados/repositorio_juegos_jugados.dart';
import 'package:tallerex1/caracteristicas/juegos_jugados/repositorio_xml.dart';
// import 'package:tallerex1/caracteristicas/repositorio_verificacion.dart';

void main() {
  test('Benthor jugo 5 juegos', () async {
    RepositorioXmlPruebas repositorioPruebas = RepositorioXmlPruebas();
    RepositorioJuegosJugadosPruebas repositorio =
        RepositorioJuegosJugadosPruebas(repositorioPruebas);
    final resultado = await repositorio
        .obtenerJuegosJugados(NickFormado.constructor('Benthor'));
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) {
      expect(r.length, equals(5));
    });
  });

//prueba de que Takenoko esta entre los jugados
  test('takenoko es de los jugados por Benthor', () async {
    RepositorioXmlPruebas repositorioPruebas = RepositorioXmlPruebas();
    RepositorioJuegosJugadosPruebas repositorio =
        RepositorioJuegosJugadosPruebas(repositorioPruebas);
    final resultado = await repositorio
        .obtenerJuegosJugados(NickFormado.constructor('Benthor'));
    final takenoko = JuegoJugado.constructor(
        nombrePropuesto: 'Takenoko', idPropuesto: '70919');
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) {
      expect(r.contains(takenoko), equals(true));
    });
  });

//prueba de que el Monopoly no esta entre los jugados
  test('Monopoly no es de los jugados por Benthor ', () async {
    RepositorioXmlPruebas repositorioPruebas = RepositorioXmlPruebas();
    RepositorioJuegosJugadosPruebas repositorio =
        RepositorioJuegosJugadosPruebas(repositorioPruebas);
    final resultado = await repositorio
        .obtenerJuegosJugados(NickFormado.constructor('Benthor'));
    final monopoly =
        JuegoJugado.constructor(nombrePropuesto: 'Monopoly', idPropuesto: '9');
    resultado.match((l) {
      //expect(true, equals(false));
      assert(false);
    }, (r) {
      expect(!r.contains(monopoly), equals(true));
    });
  });

//prueba de que me regresa un problema si busco alguien que no es Benthor
  test('si no es benthor me da error', () async {
    RepositorioXmlPruebas repositorioPruebas = RepositorioXmlPruebas();
    RepositorioJuegosJugadosPruebas repositorio =
        RepositorioJuegosJugadosPruebas(repositorioPruebas);
    final resultado =
        await repositorio.obtenerJuegosJugados(NickFormado.constructor('Pepe'));
    resultado.match((l) {
      assert(true);
    }, (r) {
      expect(!r.contains(resultado), equals(false));
    });
  });
}
