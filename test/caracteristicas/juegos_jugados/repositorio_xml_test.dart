import 'package:flutter_test/flutter_test.dart';
import 'package:tallerex1/caracteristicas/dominio/nick_formado.dart';
import 'package:tallerex1/caracteristicas/juegos_jugados/repositorio_xml.dart';

void main() {
  test('si le paso Benthor me debe regresar un solo XML', () async {
    RepositorioXml repositorio = RepositorioXmlPruebas();
    final resultado =
        await repositorio.obtenerXml(NickFormado.constructor('Benthor'));
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) {
      expect(r.length, equals(1));
    });
  });

  test('Si le paso Fokuleh me debe regresar 4 Xml', () async {
    RepositorioXml repositorio = RepositorioXmlPruebas();
    final resultado =
        await repositorio.obtenerXml(NickFormado.constructor('Fokuleh'));
    resultado.match((l) {
      expect(true, equals(false));
    }, (r) {
      expect(r.length, equals(4));
    });
  });
}
